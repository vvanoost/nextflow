profiles {
  docker {
    docker.temp = 'auto'
    docker.enabled = true
    process {
      $fastqc_fastq {
        container = "fastqc:0.11.5"
      }
      $fastqc_fastq_cutadapt {
        container = "fastqc:0.11.5"
      }
      $fastqc_fastq_randombp {
        container = "fastqc:0.11.5"
      }
      $fastqc_fastq_urqt {
        container = "fastqc:0.11.5"
      }
      $adaptor_removal {
      container = "cutadapt:1.14"
      }
      $multiqc {
        container = "multiqc:1.0"
      }
      $random_bases_4_trimming {
        container = "cutadapt:1.14"
      }
      $trimming {
        container = "urqt:d62c1f8"
      }
      $index_fasta {
        container = "bowtie2:2.3.4.1"
      }
      $mapping_fastq {
        container = "bowtie2:2.3.4.1"
      }
      $sort_bam {
        container = "samtools:1.7"
      }
      $index_bam {
        container = "samtools:1.7"
      }
    }
  }
  sge {
    process{
      $fastqc_fastq {
        beforeScript = "module purge; module load FastQC/0.11.5"
        executor = "sge"
        cpus = 1
        memory = "5GB"
        time = "6h"
        queueSize = 1000
        pollInterval = '60sec'
        queue = 'monointeldeb128'
      }
      $fastqc_fastq_cutadapt {
        beforeScript = "module purge; module load FastQC/0.11.5"
        executor = "sge"
        cpus = 1
        memory = "5GB"
        time = "6h"
        queueSize = 1000
        pollInterval = '60sec'
        queue = 'monointeldeb128'
      }
      $fastqc_fastq_randombp {
        beforeScript = "module purge; module load FastQC/0.11.5"
        executor = "sge"
        cpus = 1
        memory = "5GB"
        time = "6h"
        queueSize = 1000
        pollInterval = '60sec'
        queue = 'monointeldeb128'
      }
      $fastqc_fastq_urqt {
        beforeScript = "module purge; module load FastQC/0.11.5"
        executor = "sge"
        cpus = 1
        memory = "5GB"
        time = "6h"
        queueSize = 1000
        pollInterval = '60sec'
        queue = 'monointeldeb128'
      }
      $multiqc {
        beforeScript = "module purge; module load FastQC/1.0"
        executor = "sge"
        cpus = 1
        memory = "5GB"
        time = "6h"
        queueSize = 1000
        pollInterval = '60sec'
        queue = 'monointeldeb128'
      }
      $adaptor_removal {
        beforeScript = "module purge; module load cutadapt/1.14"
        executor = "sge"
        cpus = 1
        memory = "5GB"
        time = "6h"
        queueSize = 1000
        pollInterval = '60sec'
        queue = 'h6-E5-2667v4deb128'
        penv = 'openmp8'
      }
      $random_bases_4_trimming {
        beforeScript = "module purge; module load cutadapt/1.14"
        executor = "sge"
        cpus = 1
        memory = "5GB"
        time = "6h"
        queueSize = 1000
        pollInterval = '60sec'
        queue = 'h6-E5-2667v4deb128'
        penv = 'openmp8'
      }
      $trimming {
        beforeScript = "module purge; module load UrQt/d62c1f8"
        executor = "sge"
        cpus = 4
        memory = "5GB"
        time = "6h"
        queueSize = 1000
        pollInterval = '60sec'
        queue = 'h6-E5-2667v4deb128'
        penv = 'openmp8'
      }
      $index_fasta {
        beforeScript = "module purge; module load Bowtie2/2.3.4.1"
      }
      $mapping_fastq {
        beforeScript = "module purge; module load SAMtools/1.7; module load Bowtie2/2.3.4.1"
      }
      $sort_bam {
        beforeScript = "module purge; module load SAMtools/1.7"
      }
      $index_bam {
        beforeScript = "module purge; module load SAMtools/1.7"
      }
    }
  }
}
